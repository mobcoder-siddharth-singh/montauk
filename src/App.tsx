import React from "react";
import { AuthNavigation } from "./navigation";

function App() {
  return <AuthNavigation />;
}

export default App;
