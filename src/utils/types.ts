export type FoodDetails = {
  fdcId?: number;
  dataType?: string;
  description?: string;
  additionalDescriptions?: string;
  foodCategoryId?: number;
  foodCode?: number;
  foodCategory?: string;
};
export type FoodList = {
  fdcid: number;
  foods: FoodDetails[];
  pageList: Array<string>;
  totalHits: number;
  totalPages: number;
};
export type SearchBarProps = {
  searchText: string;
  setSearchtext: any;
};
