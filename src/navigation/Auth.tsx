import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
} from "react-router-dom";
import { HomeScreen } from "../screens";

const Auth = () => {
  return (
    <Router>
      <Routes>
        <Route  path="/" element={<HomeScreen />} />
        <Route path="*" element={<Navigate replace to="/" />} />
       
      </Routes>
    </Router>
  );
};

export default Auth;
