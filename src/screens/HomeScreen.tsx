import {
  Box,
  Pagination,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useState, useEffect, useCallback } from "react";
import { Loader, SearchBar, Wrapper } from "../components";
import { getFoodListService } from "../config/services/userService";
import { FoodDetails } from "../utils/types";
import debounce from "lodash.debounce";
import { Checkbox } from "@mui/material";
import "../index.css";

const HomeScreen = (): JSX.Element => {
  const [foodsList, setFoodsList] = useState<FoodDetails[]>();
  const [searchText, setSearchText] = useState<string>("");
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [dispText, setDispText] = useState<string>("");
  const [pageNumber, setPageNumber] = useState<number>(1);

  const [carbsCheck, setCarbsCheck] = useState<boolean>(false);
  const [protienCheck, setProtienCheck] = useState<boolean>(false);
  const [fatsCheck, setFatsCheck] = useState<boolean>(false);
  const [vitaminsCheck, setVitaminsCheck] = useState<boolean>(false);
  const [mineralsCheck, setMineralsCheck] = useState<boolean>(false);
  const [dietFibreCheck, setdietFibreCheck] = useState<boolean>(false);

  const pageSize = 10;

  useEffect(() => {
    getFoodListService(
      searchText,
      pageSize,
      pageNumber,
      mineralsCheck,
      carbsCheck,
      protienCheck,
      fatsCheck,
      vitaminsCheck,
      dietFibreCheck
    )
      .then((res) => handleData(res))
      .catch((error) => console.log("error---", error));
  }, [
    searchText,
    pageNumber,
    mineralsCheck,
    carbsCheck,
    protienCheck,
    fatsCheck,
    vitaminsCheck,
    dietFibreCheck,
  ]);

  const handleData = (data: any) => {
    setFoodsList(data?.data?.foods);
    setIsLoading(false);
  };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const debounceSearch = useCallback(
    debounce((val: string) => setSearchText(val), 1000),
    []
  );
  const handleSearchText = (data: string) => {
    debounceSearch(data);
    setDispText(data);
    setIsLoading(true);
  };
  return (
    <Wrapper>
      <Box sx={{ backgroundColor: "#f0fffa", height: "100vh" }}>
        <Box textAlign={"center"} paddingTop={"3%"} paddingBottom="2%">
          <SearchBar searchText={dispText} setSearchtext={handleSearchText} />
        </Box>
        {searchText === "" ? (
          <div
            style={{
              flexDirection: "row",
              display: "flex",
              justifyContent: "center",
            }}
          >
            <div
              style={{
                flexDirection: "row",
                display: "flex",
                alignItems: "center",
              }}
            >
              <Typography>Carbohydrates</Typography>
              <Checkbox
                checked={carbsCheck}
                onChange={(event) => setCarbsCheck(event.target.checked)}
              />
            </div>
            <Box
              sx={{
                flexDirection: "row",
                display: "flex",
                alignItems: "center",
              }}
            >
              <Typography>Protien</Typography>
              <Checkbox
                checked={protienCheck}
                onChange={(event) => setProtienCheck(event.target.checked)}
              />
            </Box>
            <Box
              sx={{
                flexDirection: "row",
                display: "flex",
                alignItems: "center",
              }}
            >
              <Typography>Minerals</Typography>
              <Checkbox
                checked={mineralsCheck}
                onChange={(event) => setMineralsCheck(event.target.checked)}
              />
            </Box>
            <Box
              sx={{
                flexDirection: "row",
                display: "flex",
                alignItems: "center",
              }}
            >
              <Typography>Fats</Typography>
              <Checkbox
                checked={fatsCheck}
                onChange={(event) => setFatsCheck(event.target.checked)}
              />
            </Box>
            <Box
              sx={{
                flexDirection: "row",
                display: "flex",
                alignItems: "center",
              }}
            >
              <Typography>Vitamins</Typography>
              <Checkbox
                checked={vitaminsCheck}
                onChange={(event) => setVitaminsCheck(event.target.checked)}
              />
            </Box>
            <Box
              sx={{
                flexDirection: "row",
                display: "flex",
                alignItems: "center",
              }}
            >
              <Typography>Dietary Fibre</Typography>
              <Checkbox
                checked={dietFibreCheck}
                onChange={(event) => setdietFibreCheck(event.target.checked)}
              />
            </Box>
          </div>
        ) : null}
        <Table stickyHeader>
          <TableHead>
            <TableCell align="left">Id</TableCell>
            <TableCell align="left">Name</TableCell>
            <TableCell align="left">FoodCode</TableCell>
            <TableCell align="left">Food Category</TableCell>
          </TableHead>
          {!isLoading ? (
            <TableBody>
              {foodsList
                ? foodsList.map((item: FoodDetails) => {
                    return (
                      <TableRow key={item.fdcId} hover>
                        <TableCell align="left">{item.fdcId}</TableCell>
                        <TableCell align="left">{item.description}</TableCell>
                        <TableCell align="left">{item.foodCode}</TableCell>
                        <TableCell align="left">{item.foodCategory}</TableCell>
                      </TableRow>
                    );
                  })
                : null}
            </TableBody>
          ) : null}
        </Table>
        {foodsList?.length && !isLoading ? (
          <Pagination
            count={10}
            onChange={(event: any, value: number) => setPageNumber(value)}
          />
        ) : null}
        {foodsList?.length === 0 ? (
          <Typography
            variant="h6"
            component="h1"
            color={"#adadad"}
            marginTop={"20%"}
            textAlign={"center"}
          >
            No Data Found !
          </Typography>
        ) : isLoading ? (
          <Box textAlign={"center"} marginTop="20%">
            <Loader width="2rem" height="2rem" borderWidth=".4rem" />
          </Box>
        ) : null}
      </Box>
    </Wrapper>
  );
};

export default HomeScreen;
