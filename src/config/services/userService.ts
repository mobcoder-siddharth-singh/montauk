import axios from "axios";
import Api from "../constants/Api";

axios.defaults.headers.common["accept"] = "application/json";
axios.defaults.headers.common["content-type"] = "multipart/form-data";

export const getFoodListService = (
  query: string,
  pageSize: number,
  pageNumber: number,
  mineralsCheck?: boolean,
  carbsCheck?: boolean,
  protienCheck?: boolean,
  fatsCheck?: boolean,
  vitaminsCheck?: boolean,
  dietFibreCheck?: boolean
) => {
  if (!query) {
    return axios.get(
      `${Api.SEARCH}?query=${carbsCheck ? "Carbohydrate" : ""}${
        protienCheck ? "Protein" : ""
      }${fatsCheck ? "Fats" : ""}${vitaminsCheck ? "Vitamin" : ""}${
        mineralsCheck ? "Mineral" : ""
      }${dietFibreCheck ? "Dietary Fibre" : ""}
      &api_key=${
        Api.KEY
      }&dataType=&pageSize=${pageSize}&pageNumber=${pageNumber}`
    );
  } else {
    return axios.get(
      `${Api.SEARCH}?query=${query}&api_key=${Api.KEY}&dataType=&pageSize=${pageSize}&pageNumber=${pageNumber}`
    );
  }
};
