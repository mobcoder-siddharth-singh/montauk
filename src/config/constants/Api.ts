const baseUrl = "https://api.nal.usda.gov/fdc";

export default (() => {
  return {
    KEY: "DucsoFFA2j8tCseqFQUva3wusvtgCxaSq0Fh8xgr",
    SEARCH: baseUrl + "/v1/foods/search",
  };
})();
