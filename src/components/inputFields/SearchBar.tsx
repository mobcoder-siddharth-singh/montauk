import { SearchBarProps } from "../../utils/types";
import { TextField } from "@mui/material";

const SearchBar = (props: SearchBarProps): JSX.Element => {
  const handleTextChange = (data: string) => {
    props.setSearchtext(data);
  };
  return (
    <TextField
      label={"Search for items"}
      value={props.searchText}
      id="standard-basic"
      onChange={(data) => handleTextChange(data.target.value)}
    />
  );
};

export default SearchBar;
