import "./Loader.css";

const Loader = ({
  width,
  height,
  borderWidth,
}: {
  width: string| number;
  height: string| number;
  borderWidth: string| number;
}): JSX.Element => {
  return (
    <>
      <div
        className="lds-ring"
        style={{ width: width, height: height, borderWidth: borderWidth }}
      >
        <div
          style={{ width: width, height: height, borderWidth: borderWidth }}
        ></div>
        <div
          style={{ width: width, height: height, borderWidth: borderWidth }}
        ></div>
        <div
          style={{ width: width, height: height, borderWidth: borderWidth }}
        ></div>
        <div
          style={{ width: width, height: height, borderWidth: borderWidth }}
        ></div>
      </div>
    </>
  );
};

export default Loader;
